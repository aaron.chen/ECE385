// sel selects between out = a3,a2,a1,a0
module mux4to1 #(parameter width = 16)
(
	input logic [1:0] sel,
	input logic [width-1:0] a3,a2,a1,a0,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 2'b00)
		out = a0;
	else if (sel == 2'b01)
		out = a1;
	else if (sel == 2'b10)
		out = a2;
	else
		out = a3;
end

endmodule : mux4to1