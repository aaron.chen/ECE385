module register #(parameter width = 16)
(
	input logic Clk,Reset,
	input logic load_enable,
	input logic [width-1:0] data_in,
	output logic [width-1:0] data_out
);

logic [width-1:0] data;



always_ff @(posedge Clk)
begin
	if(Reset)
		data<=0;
	else
	begin
		if (load_enable)
			data <= data_in;
	end
end

assign data_out = data;

endmodule : register
	