module alu #(parameter width = 16)
(
	input logic [1:0] op,
	input logic [width-1:0] b, a,
	output logic [width-1:0]data_out 
);

//temporary. Just let SR1 pass for now
always_comb
begin
    case (op)
        2'b00: data_out= a + b;
        2'b01: data_out = a & b;
        2'b10: data_out = ~a;
        2'b11: data_out= a;
		  default: $display("Unknown op");
    endcase
end

endmodule : alu