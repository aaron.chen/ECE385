module genNZP #(parameter width = 16)
(
	input logic [width-1:0] data_in,
	output logic [2:0] nzp_code
);

always_comb
begin
	if (data_in[width-1] == 1'b1)
		nzp_code = 3'b100;
	else if (data_in == {width{1'b0}})
		nzp_code = 3'b010;
	else
		nzp_code = 3'b001;
end

endmodule : genNZP