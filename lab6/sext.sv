// out = SEXT[in] to out_width bits
module sext #(parameter in_width = 4, out_width = 16)
(
	input logic [in_width-1:0] data_in,
	output logic [out_width-1:0] data_out
);

assign data_out = $signed(data_in);

endmodule : sext