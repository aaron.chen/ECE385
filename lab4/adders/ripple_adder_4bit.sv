//4 bit ripple adder
module ripple_adder_4bit
(
    input   logic[3:0]     a,
    input   logic[3:0]     b,
    input   logic          cin, // cin
    output  logic[3:0]     s, //sum
    output  logic          cout // carry out
);

//internal wire arr to represent carry ins to each adder
logic [4:0] cin_arr;
assign cin_arr[0] = cin; //first carry in is cin

//generate the rest from that first bit adder
generate
	genvar i;
	for (i = 0; i < 4; i = i + 1) begin : adder_gen
		full_adder bit_arr
		(
			.a(a[i]), .b(b[i]),
			.cin(cin_arr[i]),
			.cout(cin_arr[i+1]),
			.s(s[i])
		);
	end
endgenerate

assign cout = cin_arr[4]; // assign last carry in array to output carry

endmodule : ripple_adder_4bit
