//16 bit ripple adder
module ripple_adder
(
    input   logic[15:0]     A,
    input   logic[15:0]     B,
    output  logic[15:0]     Sum,
    output  logic           CO
);

//internal wire arr to represent carry ins to each adder
logic [16:0] cin_arr;
assign cin_arr[0] = 1'b0; //first carry in is 0

//generate the rest from that first bit adder
generate
	genvar i;
	for (i = 0; i < 16; i = i + 1) begin : adder_gen
		full_adder bit_arr
		(
			.a(A[i]), .b(B[i]),
			.cin(cin_arr[i]),
			.cout(cin_arr[i+1]),
			.s(Sum[i])
		);
	end
endgenerate

assign CO = cin_arr[16]; // assign last carry in array to output carry

endmodule : ripple_adder
