module carry_select_adder
(
    input   logic[15:0]     A,
    input   logic[15:0]     B,
    output  logic[15:0]     Sum,
    output  logic           CO
);

//internal wires to represent csa outs
logic [3:0] csa_out;

//sum of bits 3..0
ripple_adder_4bit csa0
(
	.a(A[3:0]),
	.b(B[3:0]),
	.cin(1'b0), //no initial carry
	.s(Sum[3:0]), //output to sum
	.cout(csa_out[0])
);

//sum of bits 7..4
carry_select_adder_4bit csa1
(
	.a(A[7:4]),
	.b(B[7:4]),
	.cin(csa_out[0]),
	.s(Sum[7:4]), //output to sum
	.cout(csa_out[1])
);

//sum of bits 11..8
carry_select_adder_4bit csa2
(
	.a(A[11:8]),
	.b(B[11:8]),
	.cin(csa_out[1]),
	.s(Sum[11:8]), //output to sum
	.cout(csa_out[2])
);

//sum of bits 15..12
carry_select_adder_4bit csa3
(
	.a(A[15:12]),
	.b(B[15:12]),
	.cin(csa_out[2]), //no initial carry
	.s(Sum[15:12]), //output to sum
	.cout(csa_out[3])
);

assign CO = csa_out[3];

endmodule : carry_select_adder
