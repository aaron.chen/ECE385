//Two-always example for state machine

module control (input  logic Clk, Reset, LoadA, LoadB, Execute,
                output logic Shift_En, Ld_A, Ld_B );

    // Declare signals curr_state, next_state of type enum
    // with enum values of A, B, ..., F as the state values
	 // Note that the length implies a max of 8 states, so you will need to bump this up for 8-bits
    enum logic [3:0] {start_state,shift0,shift1,shift2,shift3,shift4,shift5,shift6,shift7,stop_state}   curr_state, next_state; 

	//updates flip flop, current state is the only one
    always_ff @ (posedge Clk)  
    begin
        if (Reset)
            curr_state <= start_state;
        else 
            curr_state <= next_state;
    end

    // Assign outputs based on state
	always_comb
    begin
        
		next_state  = curr_state;	//required because I haven't enumerated all possibilities below
        unique case (curr_state)    

            start_state :    if (Execute)
                       next_state = shift0;
            shift0 :    next_state = shift1;
            shift1 :    next_state = shift2;
            shift2 :    next_state = shift3;
            shift3 :    next_state = shift4;
            shift4 :    next_state = shift5;
            shift5 :    next_state = shift6;
            shift6 :    next_state = shift7;
            shift7 :    next_state = stop_state;
            stop_state :    if (~Execute) 
                       next_state = start_state;
							  
        endcase
   
		  // Assign outputs based on ‘state’
        case (curr_state) 
	   	   start_state: 
	         begin
                Ld_A = LoadA;
                Ld_B = LoadB;
                Shift_En = 1'b0;
		      end
	   	   stop_state: 
		      begin
                Ld_A = 1'b0;
                Ld_B = 1'b0;
                Shift_En = 1'b0;
		      end
	   	   default:  //default case, can also have default assignments for Ld_A and Ld_B before case
		      begin 
                Ld_A = 1'b0;
                Ld_B = 1'b0;
                Shift_En = 1'b1;
		      end
        endcase
    end

endmodule
