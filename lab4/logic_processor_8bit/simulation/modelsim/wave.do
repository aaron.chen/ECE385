onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /testbench/Clk
add wave -noupdate -radix decimal /testbench/Reset
add wave -noupdate -radix decimal /testbench/LoadA
add wave -noupdate -radix decimal /testbench/LoadB
add wave -noupdate -radix decimal /testbench/Execute
add wave -noupdate -radix decimal /testbench/Din
add wave -noupdate -radix decimal /testbench/F
add wave -noupdate -radix decimal /testbench/R
add wave -noupdate -radix decimal /testbench/LED
add wave -noupdate -radix decimal /testbench/Aval
add wave -noupdate -radix decimal /testbench/Bval
add wave -noupdate -radix decimal /testbench/AhexL
add wave -noupdate -radix decimal /testbench/AhexU
add wave -noupdate -radix decimal /testbench/BhexL
add wave -noupdate -radix decimal /testbench/BhexU
add wave -noupdate -radix decimal /testbench/ans_1a
add wave -noupdate -radix decimal /testbench/ans_2b
add wave -noupdate -radix decimal /testbench/ErrorCnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {208661 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {759268 ps}
bookmark add wave bookmark0 {{0 ps} {759268 ps}} 0
