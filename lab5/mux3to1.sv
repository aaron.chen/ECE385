//3-1 mux (variable width), sel toggles between out a and b
module mux3to1 #(parameter width = 16)
(
	input logic [1:0] sel,
	input logic [width-1:0] a, b, c,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 2'b00)
		out = a;
	else if (sel == 2'b01)
		out = b;
	else
		out = c;
end

endmodule : mux3to1