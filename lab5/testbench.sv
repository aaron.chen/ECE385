module testbench();

timeunit 10ns;	// Half clock cycle at 50 MHz
			// This is the amount of time represented by #1 
timeprecision 1ns;

// These signals are internal because the processor will be 
// instantiated as a submodule in testbench.
//inputs to circuits

logic	Clk, Reset, ClearA_LoadB, Run;
logic [7:0]	S;
logic [7:0]	Aval, Bval;
logic [6:0]	AhexL, AhexU, BhexL, BhexU;
logic X;

// test nums
logic[15:0] t1 = 16'h006e;
logic[15:0] t2 = 16'h001a;
logic[15:0] t3 = 16'h00f7;
logic[15:0] t4 = 16'h0093;

logic[15:0] test;

integer successes = 0;
		
// Instantiating the DUT
Multiplier circuit(.*);	

// Toggle the clock
// #1 means wait for a delay of 1 timeunit
always begin : CLOCK_GENERATION
#1 Clk = ~Clk;
end

initial begin: CLOCK_INITIALIZATION
    Clk = 0;
end

// Testing begins here
// The initial block is not synthesizable
// Everything happens sequentially inside an initial block
// as in a software program
initial begin: TEST_VECTORS
//init
Reset = 0;		// reset it
Run = 1;		// don't run
ClearA_LoadB = 1; //don't load B

#2 Reset = 1; //stop reset
//TEST 1
#2 S = t1[7:0];
#2 ClearA_LoadB = 0; //clear A, load B
#2 ClearA_LoadB = 1; 
#2 S = t2[7:0];
// B = 110
// S = 26



#2 Run = 0; // get the prod
#50 Run = 1; // unswitch Run
test[15:8] = Aval;
test[7:0] = Bval;
$display("TEST 1");
$display("       B: %b", t1[7:0]);
$display("       S: %b", t2[7:0]);
$display("Calc Prod: %b", test);
$display("Real Prod: %b", t1*t2);
	if (test != (t1 * t2)) // Expected result of 1st cycle
	    $display("Test 1 failed!");
	else
		successes = successes + 1;

//TEST 2
#2 Run = 0;	// Toggle Run without doing resetting
#50 Run = 1;
//B = 44
//S = 26

test[15:8] = Aval;
test[7:0] = Bval;
$display("TEST 2");
$display("       B: %b", 8'h2c);
$display("       S: %b", t2[7:0]);
$display("Calc Prod: %b", test);
$display("Real Prod: %b", 8'h2c*t2);
	if (test != (8'h2c*t2)) // Expected result of 2nd cycle (stay same)
	    $display("Test 2 failed!");
	else
		successes = successes + 1;

//TEST 3
#2 ClearA_LoadB = 0; //clear A, load B
#2 ClearA_LoadB = 1; 
#2 S = t3[7:0];
// B = x1a
// S = xf7

#2 Run = 0; // get the Prod
#50 Run = 1; // unswitch Run
test[15:8] = Aval;
test[7:0] = Bval;
$display("TEST 3");
$display("       B: %b", t2[7:0]);
$display("       S: %b", t3[7:0]);
$display("Calc Prod: %b",  test);

//TEST 4
#2 S = t3[7:0];
#2 ClearA_LoadB = 0; //clear A, load B
#2 ClearA_LoadB = 1; 
#2 S = t1[7:0];

// B = xf7
// S = x6e

#2 Run = 0; // get the Prod
#50 Run = 1; // unswitch Run
test[15:8] = Aval;
test[7:0] = Bval;
$display("TEST 4");
$display("       B: %b", t3[7:0]);
$display("       S: %b", t1[7:0]);
$display("Calc Prod: %b", test);

//TEST 5
#2 S = t3[7:0];
#2 ClearA_LoadB = 0; //clear A, load B
#2 ClearA_LoadB = 1; 
#2 S = t4[7:0];

// B = xf7
// S = x93

#2 Run = 0; // get the Prod
#50 Run = 1; // unswitch Run
test[15:8] = Aval;
test[7:0] = Bval;
$display("TEST 5");
$display("       B: %b", t3[7:0]);
$display("       S: %b", t4[7:0]);
$display("Calc Prod: %b", test);

if (successes == 2)
	$display("SUCCESS!");
else
	$display("Something went wrong.");
end
endmodule
