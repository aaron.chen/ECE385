module control
(
	input logic Clk, Reset, ClearA_LoadB, Run, M,
	output logic Shift, Clr_Ld, Clr_XA, Clr_B, Add,
	output logic [1:0] AddMux_Sel
);
					//8 shift states and 8 add states; start and hold states
enum logic[4:0]	{shf1,shf2,shf3,shf4,shf5,shf6,shf7,shf8,start,hold,exec,add1,add2,add3,add4,add5,add6,add7,add8,reset}curr_s,next_s;
					
always_ff@(posedge Clk)
begin
	if(!Reset)
		curr_s <= reset;
	else
		curr_s <= next_s;
end
 //Assign outputs based on state
always_comb
begin
	next_s = curr_s;//Default:stay in current state unless trigerred
	case(curr_s)//holds till run
		start: 	if(Run == 1'b0)
					next_s = exec;
				else if (Reset == 1'b0)
					next_s = reset;
		reset: next_s = start;
		exec: next_s = add1;
		add1: next_s = shf1;
		shf1: next_s = add2;
		add2: next_s = shf2;
		shf2: next_s = add3;
		add3: next_s = shf3;
		shf3: next_s = add4;
		add4: next_s = shf4;
		shf4: next_s = add5;
		add5: next_s = shf5;
		shf5: next_s = add6;
		add6: next_s = shf6;
		shf6: next_s = add7;
		add7: next_s = shf7;
		shf7: next_s = add8;
		add8: next_s = shf8;
		shf8: next_s = hold;
		hold: if(Run == 1'b1)
			next_s=start;
	endcase
end

always_comb
begin
	Clr_XA = 1'b0; // by default don't clear XA
	Clr_Ld = 1'b0; // by default don't clear A and load B
	Clr_B = 1'b0; // by default don't clear B
	Shift = 1'b0; // by default don't shift
	Add = 1'b0; // by default don't add
	AddMux_Sel = 2'b01; //by default add 0 to A
	case(curr_s)
		start, hold://state:idle
		begin
			AddMux_Sel = 2'b00;
			if (ClearA_LoadB == 1'b1) //clear button not pressed
				Clr_Ld = 1'b0;
			else
				Clr_Ld = 1'b1; // if user asks to explicitly clear A, load B
		end

		reset:
		begin // reset all
			Shift = 1'b0;
			Add = 1'b0;
			Clr_Ld = 1'b0;
			Clr_XA = 1'b1;
			Clr_B = 1'b1;
			AddMux_Sel = 2'b00;
		end

		exec://start execution
		begin
			Clr_XA = 1'b1; //always clear a and X on beginnging
			AddMux_Sel = 2'b00;
		end
				
		shf1,shf2,shf3,shf4,shf5,shf6,shf7,shf8://state:shift
		begin
			Shift = 1'b1;
			Add = 1'b0;
		end

		add1,add2,add3,add4,add5,add6,add7:
		begin
			if(M == 1'b1)
				AddMux_Sel = 2'b00; //add S to A
			else
				begin
					AddMux_Sel = 2'b01; //add 0 to A
				end
			Add = 1'b1;
			Shift = 1'b0;
		end
		add8://substract if M=1
		begin
			if(M == 1'b1)
				AddMux_Sel = 2'b10; //subtract S from A
			else
				begin
					AddMux_Sel = 2'b01; //add 0 to A
				end
			Add = 1'b1;
			Shift = 1'b0;
		end
			
	endcase
end

endmodule : control		