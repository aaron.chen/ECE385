module reg_1bit(input logic Clk, s_en, s_In, Ld, Reset,
					input logic  Din,
					output logic Dout,
					output logic s_out);
						
logic data;	
logic shiftout;				
					
//initial
//begin
//	data = 1'b0;
//end
					
always_ff @(posedge Clk)
begin
	if(Reset ==1'b1)
		begin
			data = 1'b0;
			shiftout = 1'b0;
		end
	else if(Ld == 1'b1)
		begin
			data = Din;
			shiftout = Din;
		end
	else if(s_en == 1'b1)
		begin
			shiftout = data;
			data = s_In;
		end
	else
		begin
			data = data;
			shiftout = shiftout;
		end
end
assign Dout = data;
assign s_out = shiftout;

endmodule
