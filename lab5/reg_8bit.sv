module reg_8bit(input logic Clk, s_en, s_In, Ld, Reset,
					input logic [7:0] Din,
					output logic [7:0] Dout,
					output logic s_out);
						
logic [7:0] data;	
logic shiftout;				
					
//initial
//begin
//	data = 8'h00;
//end
					
always_ff @(posedge Clk)
begin
	if(Reset ==1'b1)
		begin
			data = 8'h00;
			shiftout = 1'b0;
		end
	else if(Ld == 1'b1)
		begin
			data = Din;
			shiftout = Din[0];
		end
	else if(s_en == 1'b1)
		begin
			shiftout = data[0];
			data[0] = data[1];
			data[1] = data[2];
			data[2] = data[3];
			data[3] = data[4];
			data[4] = data[5];
			data[5] = data[6];
			data[6] = data[7];
			data[7] = s_In;
		end
	else
		begin
			data = data;
			shiftout = shiftout;
		end
end
assign Dout = data;
assign s_out = shiftout;

endmodule
