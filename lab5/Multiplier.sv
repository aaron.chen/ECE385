module Multiplier
(
	//from outside
	input logic	Clk, Reset, ClearA_LoadB, Run,
	input logic [7:0]	S,
	output logic [7:0]	Aval, Bval,
	output logic [6:0]	AhexL, AhexU, BhexL, BhexU,
	output logic X
);
logic [8:0] AddMux_Out;
logic [7:0] RegA_Out;
logic RegA_SOut;
logic [7:0] RegB_Out;
logic RegB_SOut;
logic [8:0] APlusS_Out;
logic APlusS_COut;
logic RegX_SOut;

logic CtrlShiftSig;
logic CtrlClr_LdSig;
logic CtrlClr_XASig;
logic CtrlClr_BSig;
logic CtrlAddSig;
logic [1:0] CtrlAddMux_SelSig;

logic[6:0] AhexL_comb;
logic[6:0] AhexU_comb;
logic[6:0] BhexL_comb;
logic[6:0] BhexU_comb;

logic Xval;

assign X = Xval;

initial
begin
	Xval = 1'b0;
end

/* Decoders for HEX drivers and output registers
 * Note that the hex drivers are calculated one cycle after Sum so
 * that they have minimal interfere with timing (fmax) analysis.
 * The human eye can't see this one-cycle latency so it's OK. */
always_ff @(posedge Clk) begin
	
	AhexL <= AhexL_comb;
	AhexU <= AhexU_comb;
	BhexL <= BhexL_comb;
	BhexU <= BhexU_comb;
	
end

mux3to1 #(.width(9)) AddMux
(
	.sel(CtrlAddMux_SelSig),
	.a({S[7],S}),
	.b(9'h000),
	.c({~S[7],~S}),
	.out(AddMux_Out)
);

ripple_adder_9bit APlusS
(
	.a({X,Aval}),
	.b(AddMux_Out),
	.cin(CtrlAddMux_SelSig[1]), //0 in most cases, 1 if to subtract
	.s(APlusS_Out),
	.cout(APlusS_COut)
);

reg_1bit RegX
(
	.Clk(Clk),
	.s_en(CtrlShiftSig),
	.s_In(Xval),
	.Ld(CtrlAddSig),
	.Reset(CtrlClr_LdSig | CtrlClr_XASig),
	.Din(APlusS_Out[8]),
	.Dout(Xval),
	.s_out(RegX_SOut)
);

reg_8bit RegA
(
	.Clk,
	.s_en(CtrlShiftSig),
	.s_In(RegX_SOut),
	.Ld(CtrlAddSig),
	.Reset(CtrlClr_LdSig | CtrlClr_XASig),
	.Din(APlusS_Out[7:0]),
	.Dout(Aval),
	.s_out(RegA_SOut)
);

reg_8bit RegB
(
	.Clk,
	.s_en(CtrlShiftSig),
	.s_In(RegA_SOut),
	.Ld(CtrlClr_LdSig),
	.Reset(CtrlClr_BSig),
	.Din(S),
	.Dout(Bval),
	.s_out(RegB_SOut)
);

control ControlUnit
(
	.Clk,
	.Reset(Reset),
	.ClearA_LoadB(ClearA_LoadB),
	.Run(Run),
	.M(Bval[0]),
	.Shift(CtrlShiftSig),
	.Clr_Ld(CtrlClr_LdSig),
	.AddMux_Sel(CtrlAddMux_SelSig),
	.Clr_XA(CtrlClr_XASig), 
	.Clr_B(CtrlClr_BSig),
	.Add(CtrlAddSig)
);

HexDriver AhexL_inst
(
	.In0(Aval[3:0]),   // This connects the 4 least significant bits of 
					// register A to the input of a hex driver named AhexL_inst
	.Out0(AhexL_comb)
);

HexDriver AhexU_inst
(
	.In0(Aval[7:4]),
	.Out0(AhexU_comb)
);

HexDriver BhexL_inst
(
	.In0(Bval[3:0]),   // This connects the 4 least significant bits of 
					// register B to the input of a hex driver named BhexL_inst
	.Out0(BhexL_comb)
);

HexDriver BhexU_inst
(
	.In0(Bval[7:4]),
	.Out0(BhexU_comb)
);


endmodule : Multiplier