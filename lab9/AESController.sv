module AESController (
	input  logic CLK,
	input  logic RESET,
	input  logic AES_START,
	output logic AES_DONE,

	output logic State_load,
	output logic [1:0] StateMux_sel, //which output to put in state
	output logic [3:0] RoundKeyMux_sel, //which round key to use in this round
	output logic [1:0] ColReplaceMux_sel //which column to in InvMixColumns
);

logic [3:0] ExpandCounter;
logic [3:0] Next_Expand;

logic [3:0] LoopCounter;
logic [3:0] Next_Loop;

logic [1:0] ReverseColsCounter;
logic [1:0] Next_Reverse;

logic AES_in;
logic AES_load;

register #(.width(1)) AES_STATUS
(
	.clk(CLK), 
	.reset(RESET),
	.load_enable(AES_load),
	.data_in(AES_in),
	.data_out(AES_DONE)
);

enum logic [2:0] {
Wait2,
Wait1,
DecodeExpand,
DecodeLoop,
DecodeReverseCols,
DecodeEnd
}   State, Next_state;
	
always_ff@ (posedge CLK)
begin
	if (RESET)
	begin
		State <= Wait2;

		ExpandCounter <= 0;
		LoopCounter <= 0;
		ReverseColsCounter <= 0;
	end
	else 
	begin
		State <= Next_state;
		
		ExpandCounter <= Next_Expand;
		LoopCounter <= Next_Loop;
		ReverseColsCounter <= Next_Reverse;
	end
end

always_comb
begin
	State_load = 1'b0;
	StateMux_sel = 2'b00;
	RoundKeyMux_sel = 4'b0000;
	ColReplaceMux_sel = 2'b00;
	AES_in = 1'b0;
	AES_load = 1'b0;
	
	case(State)
		Wait1:
		begin
		end
		Wait2:
		begin
		end
		DecodeExpand:
		begin
			AES_in = 1'b1;
			AES_load = 1'b1;
			
			StateMux_sel = 2'b00;
			State_load = 1'b1;
		end
		DecodeLoop:
		begin
			RoundKeyMux_sel = LoopCounter;
			StateMux_sel = 2'b01;
			State_load = 1'b1;
		end
		DecodeReverseCols:
		begin
			ColReplaceMux_sel = ReverseColsCounter;

			StateMux_sel = 2'b10;
			State_load = 1'b1;
		end
		DecodeEnd:
		begin
			StateMux_sel = 2'b11;
			State_load = 1'b1;


			AES_in = 1'b1;
			AES_load = 1'b1;
		end
		default:
		begin
		end
	endcase
end

always_comb
begin
	Next_state = State;
	Next_Expand = ExpandCounter;
	Next_Loop = LoopCounter;
	Next_Reverse = ReverseColsCounter;

	case (State)
	Wait1: 
	begin
		if (AES_START == 1'b0)
		begin
			Next_state = Wait2;
			Next_Expand = 0;
			Next_Loop = 0;
			Next_Reverse = 0;
		end
		else
			Next_state = State;
	end
	Wait2: 
	begin
		if (AES_START)
		begin
			Next_state = DecodeExpand;
			Next_Expand = 0;
			Next_Loop = 0;
			Next_Reverse = 0;
		end
		else
			Next_state = State;
	end
	DecodeExpand:
	begin
		if (ExpandCounter == 4'b1010)
		begin
			Next_state = DecodeLoop;
		end
		else
		begin
			Next_state = State;
		end
		Next_Expand = ExpandCounter + 1;
	end
	DecodeLoop:
	begin
		Next_state = DecodeReverseCols;
		Next_Reverse = 0;
		Next_Loop = LoopCounter + 1;
	end
	DecodeReverseCols:
	begin
		if (LoopCounter == 4'b1001 && ReverseColsCounter == 2'b11)
			Next_state = DecodeEnd;
		else if (ReverseColsCounter == 2'b11)
			Next_state = DecodeLoop;
		else
			Next_state = State;
		Next_Reverse = ReverseColsCounter + 1;
	end
	DecodeEnd:
	begin
		Next_state = Wait1;
	end
	default:
	begin
		Next_state = Wait2;
		Next_Expand = 0;
		Next_Loop = 0;
		Next_Reverse = 0;
	end
	endcase		
end
		
endmodule : AESController
