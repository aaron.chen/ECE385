/************************************************************************
AES Decryption Core Logic

Dong Kai Wang, Fall 2017

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

module AES (
	input	 logic CLK,
	input  logic RESET,
	input  logic [127:0] AES_KEY,
	input  logic [127:0] AES_MSG_ENC,
	output logic [127:0] AES_MSG_DEC,

	input logic State_load,
	input logic [1:0] StateMux_sel, //which input to put in state
	input logic [3:0] RoundKeyMux_sel, //which round key to use in this round
	input logic [1:0] ColReplaceMux_sel //which column to in InvMixColumns
);

//internal wires
logic [127:0] StateMux_out; //input into state
logic [127:0] State_out; //output of state register update on every clock cycle when decryption is running

//before loop
logic [1407:0] KeyExpansion_out; //keyschedule
logic [127:0] AddRoundKeyInit_out; //first add round key, doesn't need mux

//loop
logic [127:0] InvShiftRowsLoop_out; //shiftrows of entire state
logic [127:0] InvSubStateLoop_out; //invert sub bytes of entire state
logic [127:0] RoundKeyMux_out; //round key to use in this round
logic [127:0] AddRoundKeyLoop_out; //apply round key on entire state
logic [31:0] ColInReplaceMux_out; //the column to replace using InvMixColumns
logic [31:0] InvMixColumnsLoop_out; //invert mix columns on one word of state
logic [127:0] ColOutReplaceMux_out; //the column concatenated with rest of word using InvMixColumns

//after loop
logic [127:0] InvShiftRowsFinal_out; //shiftrows of entire state
logic [127:0] InvSubStateFinal_out; //invert sub bytes of entire state
logic [127:0] AddRoundKeyFinal_out; //last add round key, doesn't need mux

//statemux
mux4to1 #(.width(128)) StateMux
(
	.sel(StateMux_sel),
	.a0(AddRoundKeyInit_out),
	.a1(AddRoundKeyLoop_out),
	.a2(ColOutReplaceMux_out),
	.a3(AddRoundKeyFinal_out),
	.out(StateMux_out)
);

//state, usually updates, but should not update 
register #(.width(128)) State
(
	.clk(CLK),
	.reset(RESET),
	.load_enable(State_load),
	.data_in(StateMux_out),
	.data_out(State_out)
);

//key expansion
KeyExpansion KeyExpansion
(
	.clk(CLK),
	.Cipherkey(AES_KEY),
	.KeySchedule(KeyExpansion_out)
);

//first round key, gets loaded in state in initial state
AddRoundKey AddRoundKeyInit
(
	.in(AES_MSG_ENC),
	.roundkey(KeyExpansion_out[127:0]), //use first round key
	.out(AddRoundKeyInit_out)
);


//invert shift rows in loop, changes input every cycle b/c state changes every cycle
InvShiftRows InvShiftRowsLoop
(
	.data_in(State_out),
	.data_out(InvShiftRowsLoop_out)
);

//invert sub state in loop, changes input every cycle b/c state changes every cycle
InvSubState InvSubStateLoop
(
	.clk(CLK),
	.in(InvShiftRowsLoop_out),
	.out(InvSubStateLoop_out)
);

//Need to change the Round Key on every loop cycle
mux9to1 #(.width(128)) RoundKeyMux
(
	.sel(RoundKeyMux_sel),
	.a0(KeyExpansion_out[255:128]),
	.a1(KeyExpansion_out[383:256]),
	.a2(KeyExpansion_out[511:384]),
	.a3(KeyExpansion_out[639:512]),
	.a4(KeyExpansion_out[767:640]),
	.a5(KeyExpansion_out[895:768]),
	.a6(KeyExpansion_out[1023:896]),
	.a7(KeyExpansion_out[1151:1024]),
	.a8(KeyExpansion_out[1279:1152]),
	.out(RoundKeyMux_out)
);

//Add Muxed Round key to the state
AddRoundKey AddRoundKeyLoop
(
	.in(InvSubStateLoop_out),
	.roundkey(RoundKeyMux_out), //use muxed round key
	.out(AddRoundKeyLoop_out)
);

//after round key is applied to state and updates state register
//loop will do mix columns on each word,
//so use this mux and later mux to send correct column to inv mix columns
mux4to1 #(.width(32)) ColInReplaceMux
(
	.sel(ColReplaceMux_sel),
	.a0(State_out[31:0]),
	.a1(State_out[63:32]),
	.a2(State_out[95:64]),
	.a3(State_out[127:96]),
	.out(ColInReplaceMux_out)
);

//inv mix column (one word)
InvMixColumns InvMixColumnsLoop
(
	.in(ColInReplaceMux_out),
	.out(InvMixColumnsLoop_out)
);

//output of inv mix columns will replace one word of the stat
mux4to1 #(.width(128)) ColOutReplaceMux
(
	.sel(ColReplaceMux_sel),
	.a0({State_out[127:32], InvMixColumnsLoop_out}),
	.a1({State_out[127:64], InvMixColumnsLoop_out, State_out[31:0]}),
	.a2({State_out[127:96], InvMixColumnsLoop_out, State_out[63:0]}),
	.a3({InvMixColumnsLoop_out, State_out[95:0]}),
	.out(ColOutReplaceMux_out)
);

//final invert shift rows
InvShiftRows InvShiftRowsFinal
(
	.data_in(State_out),
	.data_out(InvShiftRowsFinal_out)
);

//final invert substate
InvSubState InvSubStateFinal
(
	.clk(CLK),
	.in(InvShiftRowsFinal_out),
	.out(InvSubStateFinal_out)
);

//final round key, gets loaded in state in final state
AddRoundKey AddRoundKeyFinal
(
	.in(InvSubStateFinal_out),
	.roundkey(KeyExpansion_out[1407:1280]), //use first round key
	.out(AddRoundKeyFinal_out)
);

assign AES_MSG_DEC = State_out;

endmodule
