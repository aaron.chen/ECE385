module InvSubState
(
	input  logic clk,
	input  logic [127:0] in,
	output logic [127:0] out
);

		// InvSubBytes invsubyte_arr
		// (
		// 	.clk(clk),
		// 	.in(in[i+7:i]),
		// 	.out(out[i+7:i])
		// );

//generate SubBytes module for each byte
genvar i;
generate
	for (i = 0; i < 128; i = i + 8) begin : invsubbyte_gen
		InvSubBytes invsubyte_arr
		(
			.clk(clk),
			.in(in[i+7:i]),
			.out(out[i+7:i])
		);
	end
endgenerate

endmodule : InvSubState