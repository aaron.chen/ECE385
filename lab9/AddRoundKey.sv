module AddRoundKey
(
	input logic [127:0] in,
	input logic [127:0] roundkey,
	output logic [127:0] out
);

assign out = in ^ roundkey;

endmodule : AddRoundKey