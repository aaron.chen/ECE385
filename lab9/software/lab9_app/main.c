/************************************************************************
Lab 9 Nios Software

Dong Kai Wang, Fall 2017
Christine Chen, Fall 2013

For use with ECE 385 Experiment 9
University of Illinois ECE Department
************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "aes.h"

void expandkey(unsigned long * key, unsigned long * keyschedule);
void rotword(unsigned long* w);
void addroundkey(unsigned long * state, unsigned long * keyschedule, int beg);
void subbytes(unsigned long* word);
void substate(unsigned long* state);
void shiftrows(unsigned long* state);
void mixcolumns(unsigned long* state);
unsigned char xtime(unsigned char a);


// Pointer to base address of AES module, make sure it matches Qsys
volatile unsigned int * AES_PTR = (unsigned int *) 0x00002000;
const unsigned int nr = 10;
const unsigned int nk = 4;
const unsigned int nb = 4;

// Execution mode: 0 for testing, 1 for benchmarking
int run_mode = 0;

/** charToHex
 *  Convert a single character to the 4-bit value it represents.
 *
 *  input: a character c (e.g. 'A')
 *  output: converted 4-bit value (e.g. 0xA)
 */
char charToHex(char c)
{
	char hex = c;

	if (hex >= '0' && hex <= '9')
		hex -= '0';
	else if (hex >= 'A' && hex <= 'F')
	{
		hex -= 'A';
		hex += 10;
	}
	else if (hex >= 'a' && hex <= 'f')
	{
		hex -= 'a';
		hex += 10;
	}
	return hex;
}

/** charsToHex
 *  Convert two characters to byte value it represents.
 *  Inputs must be 0-9, A-F, or a-f.
 *
 *  input: two characters c1 and c2 (e.g. 'A' and '7')
 *  output: converted byte value (e.g. 0xA7)
 */
char charsToHex(char c1, char c2)
{
	char hex1 = charToHex(c1);
	char hex2 = charToHex(c2);
	return (hex1 << 4) + hex2;
}

// Perform AES Encryption in Software
void encrypt(unsigned char * plaintext_asc, unsigned char * key_asc, unsigned long * state, unsigned long * key)
{
	unsigned long keyschedule[nb*(nr+1)];
	unsigned long t;

	int i,j;
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 8; j++)
		{
			t <<= 4;
			t += charToHex(key_asc[8*i + j]);
		}
		key[i] = t;
		for (j = 0; j < 8; j++)
		{
			t <<= 4;
			t += charToHex(plaintext_asc[8*i + j]);
		}
		state[i] = t;
	}

	// printf("\nInitial State: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }

	expandkey(key, keyschedule);

	// printf("\nInitial State: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }

	addroundkey(state, keyschedule, 0);

	// printf("\nInitial Round Key: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", keyschedule[i]);
	// }

	int round;
	for (round = 1; round < nr; round++)
	{
		// printf("\nState at start of round %d: \n", round);
		// for(i = 0; i < 4; i++){
		// 	printf("%08lX\n", state[i]);
		// }

		substate(state);

		// printf("\nAfter subbytes: \n");
		// for(i = 0; i < 4; i++){
		// 	printf("%08lX\n", state[i]);
		// }

		shiftrows(state);

		// printf("\nAfter shiftrows: \n");
		// for(i = 0; i < 4; i++){
		// 	printf("%08lX\n", state[i]);
		// }

		mixcolumns(state);

		// printf("\nAfter mixcolumns: \n");
		// for(i = 0; i < 4; i++){
		// 	printf("%08lX\n", state[i]);
		// }

		addroundkey(state, keyschedule, round*4);

		// printf("\nRound Key: \n");
		// for(i = round*4; i < round*4+4; i++){
		// 	printf("%08lX\n", keyschedule[i]);
		// }
	}
	// printf("\nState at start of final round %d: \n", round);
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }

	substate(state);

	// printf("\nAfter subbytes: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }

	shiftrows(state);

	// printf("\nAfter shiftrows: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }

	// printf("\nRound Key: \n");
	// for(i = nr*4; i < nr*4+4; i++){
	// 	printf("%08lX\n", keyschedule[i]);
	// }

	addroundkey(state, keyschedule, nr*4);

	// printf("\nFinal State: \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", state[i]);
	// }
}

// Perform AES Decryption in Hardware
void decrypt(unsigned long * state, unsigned long * key)
{

}

int main()
{
	// Input Message and Key as 32x 8bit ASCII Characters ([33] is for NULL terminator)
	unsigned char plaintext_asc[33]; //msg
	unsigned char key_asc[33]; // round keys
	// unsigned char plaintext_asc[33] = "dbe429ca8610ea6275b100476d87a2c5"; //msg
	// unsigned char key_asc[33] = "3b280014beaac269d613a16bfdc2be03"; // round keys
	// Key and Encrypted Message in 4x 32bit Format
	unsigned long state[4];
	unsigned long key[4];

	printf("Select execution mode: 0 for testing, 1 for benchmarking: ");
	 scanf("%d", &run_mode);
//	run_mode = 0;

	if (run_mode == 0) {
		while (1) {
			int i = 0;
			printf("\nEnter plain text:\n");
			scanf("%s", plaintext_asc);
			printf("\n");
			printf("\nEnter key:\n");
			scanf("%s", key_asc);
			printf("\n");
			encrypt(plaintext_asc, key_asc, state, key);

			AES_PTR[0] = key[0];
			AES_PTR[1] = key[1];
			AES_PTR[2] = key[2];
			AES_PTR[3] = key[3];

			printf("\nEncrpted message is: \n");
			for(i = 0; i < 4; i++){
				printf("%08lX\n", state[i]);
			}
			decrypt(state, key);
			printf("\nDecrypted message is: \n");
			for(i = 0; i < 4; i++){
				printf("%08lX\n", state[i]);
			}
		}
	}
	else {
		int i = 0;
		int size_KB = 1;
		for (i = 0; i < 32; i++) {
			plaintext_asc[i] = 'a';
			key_asc[i] = 'b';
		}

		clock_t begin = clock();
		for (i = 0; i < size_KB * 128; i++)
			encrypt(plaintext_asc, key_asc, state, key);
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		double speed = size_KB / time_spent;

		printf("Software Encryption Speed: %f KB/s \n", speed);

		begin = clock();
		for (i = 0; i < size_KB * 128; i++)
			decrypt(state, key);
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		speed = size_KB / time_spent;

		printf("Hardware Encryption Speed: %f KB/s \n", speed);
	}
	return 0;
}

void expandkey(unsigned long * key, unsigned long * keyschedule)
{
	unsigned long temp_data;
	unsigned long* temp = &temp_data;
	int i = 0;

	for (i = 0; i < 4; i++)
	{
		keyschedule[i] = key[i];
	}

	// printf("\nw(1:4) : \n");
	// for(i = 0; i < 4; i++){
	// 	printf("%08lX\n", keyschedule[i]);
	// }

	i = nk;
	while (i < nb * (nr + 1))
	{
		*temp = keyschedule[i-1];
		if (i % nk == 0)
		{
			rotword(temp);

			// printf("\nAfter rotword : \n");
			// printf("%08lX\n", *temp);

			subbytes(temp);

			// printf("\nAfter subbytes : \n");
			// printf("%08lX\n", *temp);

			*temp = (*temp) ^ Rcon[i / nk];

			// printf("\nrcon(%d) : \n", i);
			// printf("%08lX\n", Rcon[i / nk]);

			// printf("\nAfter rcon xor : \n");
			// printf("%08lX\n", *temp);
		}
		keyschedule[i] = keyschedule[i - nk] ^ (*temp);

		// printf("\nw(%d) : \n", i + 1);
		// printf("%08lX\n", keyschedule[i]);

		i++;
	}
}

void rotword(unsigned long* w)
{
	unsigned long t = *w;
	t >>= 24;
	*w <<= 8;
	*w += t;
}

void addroundkey(unsigned long * state, unsigned long * keyschedule, int beg)
{
	int i;
	for (i = 0; i < 4; i++)
	{
		state[i] ^= keyschedule[beg+i];
	}
}

void subbytes(unsigned long* word)
{
	unsigned char* word_byte = (unsigned char*) word;
	word_byte[0] = aes_sbox[(int) word_byte[0]];
	word_byte[1] = aes_sbox[(int) word_byte[1]];
	word_byte[2] = aes_sbox[(int) word_byte[2]];
	word_byte[3] = aes_sbox[(int) word_byte[3]];
}

void substate(unsigned long* state)
{
	subbytes(state+0);
	subbytes(state+1);
	subbytes(state+2);
	subbytes(state+3);
}

void shiftrows(unsigned long* state)
{
	unsigned long t = state[0]; // save col 0

	//first shift
	state[0] &= 0xFF000000; //clear last 24 bits of col 0
	state[0] += (state[1] & 0x00FFFFFF); //shift col 1 last 24 bits into col 0
	state[1] &= 0xFF000000; //clear last 24 bits of col 1
	state[1] += (state[2] & 0x00FFFFFF); //shift col 2 last 24 bits into col 1
	state[2] &= 0xFF000000; //clear last 24 bits of col 1
	state[2] += (state[3] & 0x00FFFFFF); //shift col 3 last 24 bits into col 2
	state[3] &= 0xFF000000; //clear last 24 bits of col 1
	state[3] += (t & 0x00FFFFFF); //shift col 0 last 24 bits into col 3

	t = state[0]; // save col 0

	//second shift
	state[0] &= 0xFFFF0000; //clear last 16 bits of col 0
	state[0] += (state[1] & 0x0000FFFF); //shift col 1 last 16 bits into col 0
	state[1] &= 0xFFFF0000; //clear last 16 bits of col 1
	state[1] += (state[2] & 0x0000FFFF); //shift col 2 last 16 bits into col 1
	state[2] &= 0xFFFF0000; //clear last 16 bits of col 1
	state[2] += (state[3] & 0x0000FFFF); //shift col 3 last 16 bits into col 2
	state[3] &= 0xFFFF0000; //clear last 16 bits of col 1
	state[3] += (t & 0x0000FFFF); //shift col 0 last 16 bits into col 3

	t = state[0]; // save col 0

	//third shift
	state[0] &= 0xFFFFFF00; //clear last 8 bits of col 0
	state[0] += (state[1] & 0x000000FF); //shift col 1 last 8 bits into col 0
	state[1] &= 0xFFFFFF00; //clear last 8 bits of col 1
	state[1] += (state[2] & 0x000000FF); //shift col 2 last 8 bits into col 1
	state[2] &= 0xFFFFFF00; //clear last 8 bits of col 1
	state[2] += (state[3] & 0x000000FF); //shift col 3 last 8 bits into col 2
	state[3] &= 0xFFFFFF00; //clear last 8 bits of col 1
	state[3] += (t & 0x000000FF); //shift col 0 last 8 bits into col 3

}

void mixcolumns(unsigned long* state)
{
	unsigned char a0,a1,a2,a3;
	unsigned char b0,b1,b2,b3;
	int i;
	for (i=0; i < 4; i++)
	{
		a0 = (unsigned char)((state[i] & 0xFF000000) >> 24);
		a1 = (unsigned char)((state[i] & 0x00FF0000) >> 16);
		a2 = (unsigned char)((state[i] & 0x0000FF00) >> 8);
		a3 = (unsigned char)(state[i] & 0x000000FF);

		// b0 = xtime(a0)^(xtime(a1)^a1)^a2^a3;
		// b1 = a0^xtime(a1)^(xtime(a2)^a2)^a3;
		// b2 = a0^a1^xtime(a2)^(xtime(a3)^a3);
		// b3 = (xtime(a0)^a0)^a1^a2^xtime(a3);

		b0 = (gf_mul[(int) a0][0])^(gf_mul[(int) a1][1])^a2^a3;
		b1 = a0^(gf_mul[(int) a1][0])^(gf_mul[(int) a2][1])^a3;
		b2 = a0^a1^(gf_mul[(int) a2][0])^(gf_mul[(int) a3][1]);
		b3 = (gf_mul[(int) a0][1])^a1^a2^(gf_mul[(int) a3][0]);

		state[i] = b0;
		state[i] <<= 8;
		state[i] += b1;
		state[i] <<= 8;
		state[i] += b2;
		state[i] <<= 8;
		state[i] += b3;
	}
}

unsigned char xtime(unsigned char a)
{
	int toxor;
	if (a % 2 == 1)
	{
		toxor = 1;
	}
	else
	{
		toxor = 0;
	}
	a <<= 1;
	if (toxor)
	{
		return (unsigned char) (a ^ 0x1b);
	}
	else
	{
		return (unsigned char) a;
	}
}
