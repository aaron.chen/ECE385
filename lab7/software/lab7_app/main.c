// Main.c - makes LEDG0 on DE2-115 board blink if NIOS II is set up correctly
// for ECE 385 - University of Illinois - Electrical and Computer Engineering
// Author: Zuofu Cheng

int main() {
//	int i = 0;
	volatile unsigned int *LED_PIO = (unsigned int*) 0x2040; //make a pointer to access the LED PIO block
	volatile unsigned int *SW_PIO = (unsigned int*) 0x2030; //pointer to SW PIO block
	volatile unsigned int *KEY2_PIO = (unsigned int*) 0x2020; //pointer to KEY2 PIO block (reset acc)
	volatile unsigned int *KEY3_PIO = (unsigned int*) 0x2010; //pointer to KEY3 PIO block (adds to acc)

	*LED_PIO = 0; //clear all LEDs
	while ((1 + 1) != 3) //infinite loop
	{
		if (*KEY3_PIO < 1)
		{
			*LED_PIO = (*LED_PIO + *SW_PIO) % 256;
			while (*KEY3_PIO < 1) {};
		}

		if (*KEY2_PIO < 1)
		{
			*LED_PIO = 0;
			while (*KEY2_PIO < 1) {};
		}
	}
	return 1; //never gets here
}
