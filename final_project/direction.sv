import game_constants::*;

module direction (
	input logic Clk, Reset,
	input logic [15:0] keycode,
	output ball_direction current_direction
);

logic can_update;
ball_direction next_direction;

always_ff @(posedge Clk)
begin
	if(Reset)
	begin
		current_direction <= DIR_RIGHT;
		can_update <= 1'b1;
	end
	else if (keycode != 0)
	begin
		current_direction <= next_direction;
		can_update <= 1'b0;
	end
	else
	begin //key is no longer pressed
		current_direction <= next_direction;
		can_update <= 1'b1;
	end
end

always_comb
begin
	next_direction = current_direction;
	if (can_update == 1'b1)
	begin
		case(keycode[7:0])
			8'h1A:  // w (up)
			begin
				case(current_direction)
				DIR_UP:
				begin
				end
				DIR_VERYUP_RIGHT:
					next_direction = DIR_UP;
				DIR_UP_RIGHT:
					next_direction = DIR_VERYUP_RIGHT;
				DIR_UP_VERYRIGHT:
					next_direction = DIR_UP_RIGHT;
				DIR_RIGHT:
					next_direction = DIR_UP_VERYRIGHT;
				DIR_DOWN_VERYRIGHT:
					next_direction = DIR_RIGHT;
				DIR_DOWN_RIGHT:
					next_direction = DIR_DOWN_VERYRIGHT;
				DIR_VERYDOWN_RIGHT:
					next_direction = DIR_DOWN_RIGHT;
				DIR_DOWN:
				begin
					next_direction = DIR_VERYDOWN_LEFT;
				end
				DIR_VERYDOWN_LEFT:
					next_direction = DIR_DOWN_LEFT;
				DIR_DOWN_LEFT:
					next_direction = DIR_DOWN_VERYLEFT;
				DIR_DOWN_VERYLEFT:
					next_direction = DIR_LEFT;
				DIR_LEFT:
					next_direction = DIR_UP_VERYLEFT;
				DIR_UP_VERYLEFT:
					next_direction = DIR_UP_LEFT;
				DIR_UP_LEFT:
					next_direction = DIR_VERYUP_LEFT;
				DIR_VERYUP_LEFT:
					next_direction = DIR_UP;
				endcase
			end
			8'h16:  // s (down)
			begin
				case(current_direction)
				DIR_UP:
				begin
					next_direction = DIR_VERYUP_RIGHT;
				end
				DIR_VERYUP_RIGHT:
					next_direction = DIR_UP_RIGHT;
				DIR_UP_RIGHT:
					next_direction = DIR_UP_VERYRIGHT;
				DIR_UP_VERYRIGHT:
					next_direction = DIR_RIGHT;
				DIR_RIGHT:
					next_direction = DIR_DOWN_VERYRIGHT;
				DIR_DOWN_VERYRIGHT:
					next_direction = DIR_DOWN_RIGHT;
				DIR_DOWN_RIGHT:
					next_direction = DIR_VERYDOWN_RIGHT;
				DIR_VERYDOWN_RIGHT:
					next_direction = DIR_DOWN;
				DIR_DOWN:
				begin
				end
				DIR_VERYDOWN_LEFT:
					next_direction = DIR_DOWN;
				DIR_DOWN_LEFT:
					next_direction = DIR_VERYDOWN_LEFT;
				DIR_DOWN_VERYLEFT:
					next_direction = DIR_DOWN_LEFT;
				DIR_LEFT:
					next_direction = DIR_DOWN_VERYLEFT;
				DIR_UP_VERYLEFT:
					next_direction = DIR_LEFT;
				DIR_UP_LEFT:
					next_direction = DIR_UP_VERYLEFT;
				DIR_VERYUP_LEFT:
					next_direction = DIR_UP_LEFT;
				endcase
			end
			8'h07:  // d (right)
			begin
				case(current_direction)
				DIR_UP:
					next_direction = DIR_VERYUP_RIGHT;
				DIR_VERYUP_RIGHT:
					next_direction = DIR_UP_RIGHT;
				DIR_UP_RIGHT:
					next_direction = DIR_UP_VERYRIGHT;
				DIR_UP_VERYRIGHT:
					next_direction = DIR_RIGHT;
				DIR_RIGHT:
				begin
				end
				DIR_DOWN_VERYRIGHT:
					next_direction = DIR_RIGHT;
				DIR_DOWN_RIGHT:
					next_direction = DIR_DOWN_VERYRIGHT;
				DIR_VERYDOWN_RIGHT:
					next_direction = DIR_DOWN_RIGHT;
				DIR_DOWN:
					next_direction = DIR_VERYDOWN_RIGHT;
				DIR_VERYDOWN_LEFT:
					next_direction = DIR_DOWN;
				DIR_DOWN_LEFT:
					next_direction = DIR_VERYDOWN_LEFT;
				DIR_DOWN_VERYLEFT:
					next_direction = DIR_DOWN_LEFT;
				DIR_LEFT:
				begin
					next_direction = DIR_UP_VERYLEFT;
				end
				DIR_UP_VERYLEFT:
					next_direction = DIR_UP_LEFT;
				DIR_UP_LEFT:
					next_direction = DIR_VERYUP_LEFT;
				DIR_VERYUP_LEFT:
					next_direction = DIR_UP;
				endcase
			end
			8'h04:  // a (left)
			begin
				case(current_direction)
				DIR_UP:
					next_direction = DIR_VERYUP_LEFT;
				DIR_VERYUP_RIGHT:
					next_direction = DIR_UP;
				DIR_UP_RIGHT:
					next_direction = DIR_VERYUP_RIGHT;
				DIR_UP_VERYRIGHT:
					next_direction = DIR_UP_RIGHT;
				DIR_RIGHT:
				begin
					next_direction = DIR_DOWN_VERYRIGHT;
				end
				DIR_DOWN_VERYRIGHT:
					next_direction = DIR_DOWN_RIGHT;
				DIR_DOWN_RIGHT:
					next_direction = DIR_VERYDOWN_RIGHT;
				DIR_VERYDOWN_RIGHT:
					next_direction = DIR_DOWN;
				DIR_DOWN:
					next_direction = DIR_VERYDOWN_LEFT;
				DIR_VERYDOWN_LEFT:
					next_direction = DIR_DOWN_LEFT;
				DIR_DOWN_LEFT:
					next_direction = DIR_DOWN_VERYLEFT;
				DIR_DOWN_VERYLEFT:
					next_direction = DIR_LEFT;
				DIR_LEFT:
				begin
				end
				DIR_UP_VERYLEFT:
					next_direction = DIR_LEFT;
				DIR_UP_LEFT:
					next_direction = DIR_UP_VERYLEFT;
				DIR_VERYUP_LEFT:
					next_direction = DIR_UP_LEFT;
				endcase
			end
		endcase
	end
end

endmodule