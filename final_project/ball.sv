import game_constants::*;

//-------------------------------------------------------------------------
//    Ball.sv                                                            --
//    Viral Mehta                                                        --
//    Spring 2005                                                        --
//                                                                       --
//    Modified by Stephen Kempf 03-01-2006                               --
//                              03-12-2007                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Modified by Po-Han Huang  10-06-2017                               --
//    Fall 2017 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 8                                         --
//    UIUC ECE Department                                                --
//-------------------------------------------------------------------------

module ball #(parameter ball_no = 0)
(
	input logic Clk,                // 50 MHz clock
	input logic Reset,              // Active-high reset signal
	input logic Start,              
	input logic frame_clk,          // The clock indicating a new frame (~60Hz)
	input [9:0] DrawX, DrawY,       // Current pixel coordinates
	output logic draw_ball,         // Whether current pixel belongs to ball or background
	input [15:0] keycode,
	input logic [3:0] user1_ball,

	input logic [9:0] spawn_X_Pos,
	input logic [9:0] spawn_Y_Pos,


	output logic [9:0] Ball_X_Pos,
	output logic [9:0] Ball_Y_Pos
);

ball_direction current_direction;

direction ball_direction_unit
(
	.Clk(Clk),
	.Reset(Reset),
	.keycode(keycode),
	.current_direction(current_direction)
);
	
logic [9:0] Ball_X_Motion, Ball_Y_Motion;
logic [9:0] Ball_X_Pos_in, Ball_X_Motion_in, Ball_Y_Pos_in, Ball_Y_Motion_in;

/* Since the multiplicants are required to be signed, we have to first cast them
	from logic to int (signed by default) before they are multiplied. */
int DistX, DistY, Size;
assign DistX = DrawX - Ball_X_Pos;
assign DistY = DrawY - Ball_Y_Pos;
assign Size = BALL_SIZE;
	
//////// Do not modify the always_ff blocks. ////////
// Detect rising edge of frame_clk
logic frame_clk_delayed;
logic frame_clk_rising_edge;
always_ff @ (posedge Clk) begin
	frame_clk_delayed <= frame_clk;
end
assign frame_clk_rising_edge = (frame_clk == 1'b1) && (frame_clk_delayed == 1'b0);

// Update ball position and motion
always_ff @ (posedge Clk)
begin
	if (Reset)
	begin
		Ball_X_Pos <= BALL_X_MAX;
		Ball_Y_Pos <= BALL_Y_CENTER;
		Ball_X_Motion <= (~(BALL_X_STEP) + 1'b1);
		Ball_Y_Motion <= 10'd0;
	end
	else if (Start)
	begin
		Ball_X_Pos <= spawn_X_Pos;
		Ball_Y_Pos <= spawn_Y_Pos;
		Ball_X_Motion <= (~(BALL_X_STEP) + 1'b1);
		Ball_Y_Motion <= 10'd0;
	end
	else if (frame_clk_rising_edge)        // Update only at rising edge of frame clock
	begin
		Ball_X_Pos <= Ball_X_Pos_in;
		Ball_Y_Pos <= Ball_Y_Pos_in;
		Ball_X_Motion <= Ball_X_Motion_in;
		Ball_Y_Motion <= Ball_Y_Motion_in;
	end
	// By defualt, keep the register values.
end

always_comb
begin
	// Update the ball's position with its motion
	Ball_Y_Pos_in = Ball_Y_Pos + Ball_Y_Motion;
	Ball_X_Pos_in = Ball_X_Pos + Ball_X_Motion;
	
	// By default, keep motion unchanged
	Ball_Y_Motion_in = Ball_Y_Motion;
	Ball_X_Motion_in = Ball_X_Motion;
	
	if (user1_ball == ball_no)
	begin
		case(current_direction)
			DIR_UP:  // w (up)
			begin
				Ball_X_Motion_in = 10'd0; //always clear first!
				// if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
				// begin
				// 	Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
				// begin
				// 	Ball_Y_Motion_in = BALL_Y_STEP;
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
				// begin
				// 	Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
				// 	Ball_Y_Motion_in = 0;
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
				// begin
				// 	Ball_X_Motion_in = BALL_X_STEP;
				// 	Ball_Y_Motion_in = 0;
				// end
				// else
					Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
			end
			DIR_VERYUP_RIGHT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b10); // - 2
				Ball_X_Motion_in = BALL_X_STEP - 2'b10; // 1
			end
			DIR_UP_RIGHT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b10); // -2
				Ball_X_Motion_in = BALL_X_STEP - 2'b01; // 2
			end
			DIR_UP_VERYRIGHT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b11); // -1
				Ball_X_Motion_in = BALL_X_STEP - 2'b01; // 2
			end
			DIR_RIGHT:  // d (right)
			begin
				Ball_Y_Motion_in = 10'd0; //always clear first!
				// if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
				// begin
				// 	Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
				// begin
				// 	Ball_X_Motion_in = BALL_X_STEP;
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
				// begin
				// 	Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
				// 	Ball_X_Motion_in = 0;
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
				// begin
				// 	Ball_Y_Motion_in = BALL_Y_STEP;
				// 	Ball_X_Motion_in = 0;
				// end
				// else
					Ball_X_Motion_in = BALL_X_STEP; 
			end	
			DIR_DOWN_VERYRIGHT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b10; // 1
				Ball_X_Motion_in = BALL_X_STEP - 2'b01; // 2
			end
			DIR_DOWN_RIGHT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b01; // 2
				Ball_X_Motion_in = BALL_X_STEP - 2'b01; // 2
			end
			DIR_VERYDOWN_RIGHT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b01; // 2
				Ball_X_Motion_in = BALL_X_STEP - 2'b10; // 1
			end
			DIR_DOWN:  // s (down)
			begin
				Ball_X_Motion_in = 10'd0; //always clear first!
				// if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
				// begin
				// 	Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
				// begin
				// 	Ball_Y_Motion_in = BALL_Y_STEP;
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
				// begin
				// 	Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
				// 	Ball_Y_Motion_in = 0;
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
				// begin
				// 	Ball_X_Motion_in = BALL_X_STEP;
				// 	Ball_Y_Motion_in = 0;
				// end
				// else
					Ball_Y_Motion_in = BALL_Y_STEP;
			end
			DIR_VERYDOWN_LEFT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b01; // 2
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b11); // -1
			end
			DIR_DOWN_LEFT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b01; // 2
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b10); // -2
			end
			DIR_DOWN_VERYLEFT:
			begin
				Ball_Y_Motion_in = BALL_Y_STEP - 2'b10; // 1
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b10); // -2
			end
			DIR_LEFT:  // a (left)
			begin
				Ball_Y_Motion_in = 10'd0; //always clear first!
				// if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
				// begin
				// 	Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
				// end
				// else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
				// begin
				// 	Ball_X_Motion_in = BALL_X_STEP;
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
				// begin
				// 	Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
				// 	Ball_X_Motion_in = 0;
				// end
				// else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
				// begin
				// 	Ball_Y_Motion_in = BALL_Y_STEP;
				// 	Ball_X_Motion_in = 0;
				// end
				// else
					Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1); 
			end
			DIR_UP_VERYLEFT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b11); // -1
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b10); // -2
			end
			DIR_UP_LEFT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b10); // -2
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b10); // -2
			end
			DIR_VERYUP_LEFT:
			begin
				Ball_Y_Motion_in = (~(BALL_Y_STEP) + 2'b10); // -2
				Ball_X_Motion_in = (~(BALL_X_STEP) + 2'b11); // -1
			end
			default:
			begin
				if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
				begin
					Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
					Ball_Y_Motion_in = 0;
				end
				else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
				begin
					Ball_X_Motion_in = BALL_X_STEP;
					Ball_Y_Motion_in = 0;
				end
				else if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
				begin
					Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
					Ball_X_Motion_in = 0;
				end
				else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
				begin
					Ball_Y_Motion_in = BALL_Y_STEP;
					Ball_X_Motion_in = 0;
				end
			end
		endcase
	end
	else
	begin
		if ((Ball_X_Pos + Ball_X_Motion + BALL_SIZE) >= BALL_X_MAX)
		begin
			Ball_X_Motion_in = (~(BALL_X_STEP) + 1'b1);
			Ball_Y_Motion_in = 0;
		end
		else if ((Ball_X_Pos + Ball_X_Motion) <= (BALL_X_MIN + BALL_SIZE))
		begin
			Ball_X_Motion_in = BALL_X_STEP;
			Ball_Y_Motion_in = 0;
		end
		else if ((Ball_Y_Pos + Ball_Y_Motion + BALL_SIZE) >= BALL_Y_MAX)
		begin
			Ball_Y_Motion_in = (~(BALL_Y_STEP) + 1'b1);
			Ball_X_Motion_in = 0;
		end
		else if ((Ball_Y_Pos + Ball_Y_Motion) <= (BALL_Y_MIN + BALL_SIZE))
		begin
			Ball_Y_Motion_in = BALL_Y_STEP;
			Ball_X_Motion_in = 0;
		end
	end
	
	// Compute whether the pixel corresponds to ball or background
	if (((DistX * DistX) + (DistY * DistY)) <= (Size * Size) ) 
		draw_ball = 1'b1;
	else
		draw_ball = 1'b0;
end

endmodule