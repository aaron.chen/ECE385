module testbench();

timeunit 10ns;	// Half clock cycle at 50 MHz
			// This is the amount of time represented by #1 
timeprecision 10ns;

logic Clk, Reset;
logic Start;
logic [5:0] seconds_left;
logic Done;
		
// Instantiating the DUT
timer dut(.*);

// Toggle the clock
// #1 means wait for a delay of 1 timeunit
always begin : CLOCK_GENERATION
#1 Clk = ~Clk;
end

initial begin: CLOCK_INITIALIZATION
    Clk = 0;
end

// Testing begins here
// The initial block is not synthesizable
// Everything happens sequentially inside an initial block
// as in a software program
initial begin: TEST_VECTORS
	
	#2 Reset = 1;
	#2 Reset = 0;

	#5 Start = 1;

	#6 Start = 0;

	#70 Start = 1;
end

endmodule