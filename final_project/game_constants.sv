package game_constants;

//TYPES
typedef logic [DIGIT_NUM_PIXELS - 1:0] decimal_digit;

//GAMEPLAY CONSTANTS
parameter MAX_SCORE = 999;

//VGA CONSTANTS
// 800 pixels per line (including front/back porch
// 525 lines per frame (including front/back porch)
parameter H_TOTAL = 10'd800;
parameter V_TOTAL = 10'd525;

//PIXEL CONSTANTS
parameter [9:0] BALL_X_CENTER = 320;  // Center position on the X axis
parameter [9:0] BALL_Y_CENTER = 240;  // Center position on the Y axis
parameter [9:0] BALL_X_MIN = 0;       // Leftmost point on the X axis
parameter [9:0] BALL_X_MAX = 639;     // Rightmost point on the X axis
parameter [9:0] BALL_Y_MIN = 0;       // Topmost point on the Y axis
parameter [9:0] BALL_Y_MAX = 479;     // Bottommost point on the Y axis

parameter [9:0] BALL_X_STEP = 3;      // Step size on the X axis
parameter [9:0] BALL_Y_STEP = 3;      // Step size on the Y axis
parameter [9:0] BALL_SIZE = 50;

parameter DIGIT_WIDTH = 40;
parameter DIGIT_HEIGHT = 60;

parameter DIGIT_NUM_PIXELS = DIGIT_WIDTH * DIGIT_HEIGHT;

typedef enum bit [3:0] {
	DIR_UP,
	DIR_VERYUP_RIGHT,
	DIR_UP_RIGHT,
	DIR_UP_VERYRIGHT,
	DIR_RIGHT,
	DIR_DOWN_VERYRIGHT,
	DIR_DOWN_RIGHT,
	DIR_VERYDOWN_RIGHT,
	DIR_DOWN,
	DIR_VERYDOWN_LEFT,
	DIR_DOWN_LEFT,
	DIR_DOWN_VERYLEFT,
	DIR_LEFT,
	DIR_UP_VERYLEFT,
	DIR_UP_LEFT,
	DIR_VERYUP_LEFT
} ball_direction;

endpackage : game_constants
