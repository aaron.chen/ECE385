module game(
	input logic CLOCK_50,
	input logic[3:0] KEY,          //bit 0 is set up as Reset
	output logic [6:0]  HEX0, HEX1,HEX2, HEX3,
	// VGA Interface 
	output logic [7:0]  VGA_R,        //VGA Red
						VGA_G,        //VGA Green
						VGA_B,        //VGA Blue
	output logic VGA_CLK,      //VGA Clock
				VGA_SYNC_N,   //VGA Sync signal
				VGA_BLANK_N,  //VGA Blank signal
				VGA_VS,       //VGA virtical sync signal
				VGA_HS,       //VGA horizontal sync signal
	// CY7C67200 Interface
	inout  wire  [15:0] OTG_DATA,     //CY7C67200 Data bus 16 Bits
	output logic [1:0]  OTG_ADDR,     //CY7C67200 Address 2 Bits
	output logic OTG_CS_N,     //CY7C67200 Chip Select
				OTG_RD_N,     //CY7C67200 Write
				OTG_WR_N,     //CY7C67200 Read
				OTG_RST_N,    //CY7C67200 Reset
	input logic OTG_INT,      //CY7C67200 Interrupt
	// SDRAM Interface for Nios II Software
	output logic [12:0] DRAM_ADDR,    //SDRAM Address 13 Bits
	inout  wire  [31:0] DRAM_DQ,      //SDRAM Data 32 Bits
	output logic [1:0]  DRAM_BA,      //SDRAM Bank Address 2 Bits
	output logic [3:0]  DRAM_DQM,     //SDRAM Data Mast 4 Bits
	output logic DRAM_RAS_N,   //SDRAM Row Address Strobe
				DRAM_CAS_N,   //SDRAM Column Address Strobe
				DRAM_CKE,     //SDRAM Clock Enable
				DRAM_WE_N,    //SDRAM Write Enable
				DRAM_CS_N,    //SDRAM Chip Select
				DRAM_CLK      //SDRAM Clock
);
	
logic Reset_h, Reset_b, Clk;
logic [15:0] keycode;
	
assign Clk = CLOCK_50;
always_ff @ (posedge Clk) begin
	Reset_h <= ~(KEY[0]);        // The push buttons are active low
end
always_ff @ (posedge Clk) begin
	Reset_b <= ~(KEY[1]);        // The push buttons are active low
end 
	
logic [1:0] hpi_addr;
logic [15:0] hpi_data_in, hpi_data_out;
logic hpi_r, hpi_w, hpi_cs;
	
// Interface between NIOS II and EZ-OTG chip
hpi_io_intf hpi_io_inst(
	.Clk(Clk),
	.Reset(Reset_h),
	// signals connected to NIOS II
	.from_sw_address(hpi_addr),
	.from_sw_data_in(hpi_data_in),
	.from_sw_data_out(hpi_data_out),
	.from_sw_r(hpi_r),
	.from_sw_w(hpi_w),
	.from_sw_cs(hpi_cs),
	// signals connected to EZ-OTG chip
	.OTG_DATA(OTG_DATA),    
	.OTG_ADDR(OTG_ADDR),    
	.OTG_RD_N(OTG_RD_N),    
	.OTG_WR_N(OTG_WR_N),    
	.OTG_CS_N(OTG_CS_N),    
	.OTG_RST_N(OTG_RST_N)
);
	 
// You need to make sure that the port names here match the ports in Qsys-generated codes.
nios_system nios_system(
	.clk_clk(Clk),         
	.reset_reset_n(1'b1),    // Never reset NIOS
	.sdram_wire_addr(DRAM_ADDR), 
	.sdram_wire_ba(DRAM_BA),   
	.sdram_wire_cas_n(DRAM_CAS_N),
	.sdram_wire_cke(DRAM_CKE),  
	.sdram_wire_cs_n(DRAM_CS_N), 
	.sdram_wire_dq(DRAM_DQ),   
	.sdram_wire_dqm(DRAM_DQM),  
	.sdram_wire_ras_n(DRAM_RAS_N),
	.sdram_wire_we_n(DRAM_WE_N), 
	.sdram_clk_clk(DRAM_CLK),
	.keycode_export(keycode),  
	.otg_hpi_address_export(hpi_addr),
	.otg_hpi_data_in_port(hpi_data_in),
	.otg_hpi_data_out_port(hpi_data_out),
	.otg_hpi_cs_export(hpi_cs),
	.otg_hpi_r_export(hpi_r),
	.otg_hpi_w_export(hpi_w)
);
  
// Use PLL to generate the 25MHZ VGA_CLK. Do not modify it.
// vga_clk vga_clk_instance(
//     .clk_clk(Clk),
//     .reset_reset_n(1'b1),
//     .altpll_0_c0_clk(VGA_CLK),
//     .altpll_0_areset_conduit_export(),    
//     .altpll_0_locked_conduit_export(),
//     .altpll_0_phasedone_conduit_export()
// );
always_ff @ (posedge Clk) begin
	if(Reset_h)
		VGA_CLK <= 1'b0;
	else
		VGA_CLK <= ~VGA_CLK;
end
	 
logic [9:0] DrawX, DrawY;
	
VGA_controller vga_controller_instance(
	.Clk,         // 50 MHz clock
	.Reset(Reset_h),       // Active-high reset signal
	.VGA_HS,      // Horizontal sync pulse.  Active low
	.VGA_VS,      // Vertical sync pulse.  Active low
	.VGA_CLK,     // 25 MHz VGA clock input
	.VGA_BLANK_N, // Blanking interval indicator.  Active low.
	.VGA_SYNC_N,  // Composite Sync signal.  Active low.  We don't use it in this lab,
					// but the video DAC on the DE2 board requires an input for it.
	.DrawX,       // horizontal coordinate
	.DrawY 
);

logic Start;
logic [8:0] exist_arr;
logic [9:0] spawn_X_Pos [8:0];
logic [9:0] spawn_Y_Pos [8:0];

game_controller ctrl
(
	.Clk,                // 50 MHz clock
	.Reset(Reset_b),              // Active-high reset signal
	.Start,             

	.exist_arr,

	.spawn_X_Pos,
	.spawn_Y_Pos
);
	
logic [8:0] draw_user1_ball_arr;
logic [9:0] Ball_X_Pos, Ball_Y_Pos;

genvar i;
generate
	for (i = 0; i < 9; i++)
	begin : ball_generator
		ball #(.ball_no(i)) ball_arr
		(
			.Clk,                // 50 MHz clock
			.Reset(Reset_b),              // Active-high reset signal
			.frame_clk(VGA_VS),  // The clock indicating a new frame (~60Hz)
			.DrawX,
			.DrawY,       // Current pixel coordinates
			.draw_ball(draw_user1_ball_arr[i]),          // Whether current pixel belongs to ball or background
			.keycode,
			.user1_ball(user1_ball_out),
			.Ball_X_Pos(position_x_arr[i]),
			.Ball_Y_Pos(position_y_arr[i]),

			.spawn_X_Pos(spawn_X_Pos[i]),
			.spawn_Y_Pos(spawn_Y_Pos[i])
		);
	end
endgenerate

logic [9:0] position_x_arr [8:0];
logic [9:0] position_y_arr [8:0];
logic [8:0] collision_arr;
logic collide_x, collide_y;
logic collided;

always_comb
begin
	collision_arr = 0;
	collide_x = 1'b0;
	collide_y = 1'b0;
	collided = 1'b0;
	for (int i = 0; i < 9; i++)
	begin
		collided = 1'b0;
		
		for (int j = 0; j < 9; j++)
		begin
			collide_x = 1'b0;
			collide_y = 1'b0;

			if (position_x_arr[i] > position_x_arr[j] && position_x_arr[i] - position_x_arr[j] <= BALL_SIZE/2)
			begin
				collide_x = 1'b1;
			end
			else if (position_x_arr[j] > position_x_arr[i] && position_x_arr[j] - position_x_arr[i] <= BALL_SIZE/2)
			begin
				collide_x = 1'b1;
			end

			if (position_y_arr[i] > position_y_arr[j] && position_y_arr[i] - position_y_arr[j] <= BALL_SIZE/2)
			begin
				collide_y = 1'b1;
			end
			else if (position_y_arr[j] > position_y_arr[i] && position_y_arr[j] - position_y_arr[i] <= BALL_SIZE/2)
			begin
				collide_y = 1'b1;
			end

			if (collide_x == 1'b1 && collide_y == 1'b1 && exist_arr[i] == 1'b1 && exist_arr[j] == 1'b1 && i != j)
			begin
				collided = 1'b1;
			end
		end
		if (collided == 1'b1)
		begin
			collision_arr[i] = 1'b1;
		end
		else
		begin
			collision_arr[i] = 1'b0;
		end

	end
end

logic user1_ball_load;
logic [3:0] user1_ball_in;
logic [3:0] user1_ball_out;
	 
register #(.width(4)) user1_ball_reg
(
	.Clk,
	.Reset(Reset_b),
	.Load(user1_ball_load),
	.data_in(user1_ball_in),
	.data_out(user1_ball_out)
);

//keys for loading chosen user1 ball
always_comb
begin
	case (keycode[7:0])
		8'h1e:
		begin
			user1_ball_in = 0;
			user1_ball_load = 1;
		end
		8'h1f:
		begin
			user1_ball_in = 1;
			user1_ball_load = 1;
		end
		8'h20:
		begin
			user1_ball_in = 2;
			user1_ball_load = 1;
		end
		8'h21:
		begin
			user1_ball_in = 3;
			user1_ball_load = 1;
		end
		8'h22:
		begin
			user1_ball_in = 4;
			user1_ball_load = 1;
		end
		8'h23:
		begin
			user1_ball_in = 5;
			user1_ball_load = 1;
		end
		8'h24:
		begin
			user1_ball_in = 6;
			user1_ball_load = 1;
		end
		8'h25:
		begin
			user1_ball_in = 7;
			user1_ball_load = 1;
		end
		8'h26:
		begin
			user1_ball_in = 8;
			user1_ball_load = 1;
		end
		default:
		begin
			user1_ball_in = 0;
			user1_ball_load = 0;
		end
	endcase
end
	
color_mapper color_instance(
	.Clk,
	.draw_user1_ball_arr(draw_user1_ball_arr),
	.collision_arr,
	.exist_arr,

	.position_x_arr,
	.position_y_arr,

	//   or background (computed in ball.sv)
	.DrawX,
	.DrawY,       // Current pixel coordinates
	.VGA_R,
	.VGA_G,
	.VGA_B // VGA RGB output
);
	
// Display keycode on hex display
HexDriver hex_inst_0 (keycode[3:0], HEX0);
HexDriver hex_inst_1 (keycode[7:4], HEX1);
HexDriver hex_inst_2 (keycode[11:8], HEX2);
HexDriver hex_inst_3 (keycode[15:12], HEX3);
	
endmodule
