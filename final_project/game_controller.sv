import game_constants::*;

module game_controller (
	input logic Clk,                // 50 MHz clock
	input logic Reset,              // Active-high reset signal
	output logic Start,             

	output logic [8:0] exist_arr,

	output logic [9:0] spawn_X_Pos [8:0],
	output logic [9:0] spawn_Y_Pos [8:0]
);

logic [8:0] exist_arr_in;
logic exist_arr_load;
logic game_over;

enum logic [2:0] {
	s_start_l1,
	s_game_l1,
	s_end_l1
} state, next_state;

register #(.width(9)) exist_arr_reg
(
	.Clk,
	.Reset,
	.Load(exist_arr_load),
	.data_in(exist_arr_in),
	.data_out(exist_arr)
);

always_ff @(posedge Clk)
begin
	if(Reset)
	begin
		state <= s_start_l1;
	end
	else
	begin
		state <= next_state;
	end
end

always_comb
begin
	game_over = 1'b0;
	exist_arr_in = 9'd0;
	exist_arr_load = 1'b0;

	for (int i = 0; i < 9; i++)
	begin
		spawn_X_Pos[i] = 0;
		spawn_Y_Pos[i] = 0;
	end

	Start = 1'b0;

	case(state)
	s_start_l1:
	begin
		Start = 1'b1;
		spawn_X_Pos[0] = BALL_X_MIN;
		spawn_Y_Pos[0] = BALL_Y_CENTER;

		exist_arr_in = 9'b000000001;
		exist_arr_load = 1'b1;
	end
	s_game_l1:
	begin
	end
	s_end_l1:
	begin
	end
	endcase
end

always_comb
begin
	next_state = state;
	case(state)
	s_start_l1:
	begin
		next_state = s_game_l1;
	end
	s_game_l1:
	begin
		if (game_over)
			next_state = s_end_l1;
		else
			next_state = state;
	end
	s_end_l1:
	begin
		next_state = s_start_l1;
	end
	endcase
end

endmodule