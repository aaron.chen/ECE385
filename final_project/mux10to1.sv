//out = ax if sel = x
module mux10to1 #(parameter width = 8)
(
	input logic [3:0] sel,
	input logic [width-1:0] a9, a8, a7, a6, a5, a4, a3, a2, a1, a0,
	output logic [width-1:0] out
);

always_comb
begin
	if (sel == 4'b0000)
		out = a0;
	else if (sel == 4'b0001)
		out = a1;
	else if (sel == 4'b0010)
		out = a2;
	else if (sel == 4'b0011)
		out = a3;
	else if (sel == 4'b0100)
		out = a4;
	else if (sel == 4'b0101)
		out = a5;
	else if (sel == 4'b0110)
		out = a6;
	else if (sel == 4'b0111)
		out = a7;
	else if (sel == 4'b1000)
		out = a8;
	else
		out = a9;
end

endmodule : mux10to1