//starts a timer for however many seconds when start = 1
//done = 1 when timer is done
//start needs be set to 0 before the timer can start again, or reset needs to be high

// CLK MUST BE 50 MHz!!!

module timer #(parameter seconds = 60)
(
	input logic Clk, Reset,
	input logic Start,
	output logic [5:0] seconds_left,
	output logic Done
);

parameter cycles = seconds * 5 * 10000000; //20 ns per cycle, 1s/(20 ns) cycles per sec = 5*10^7
logic [31:0] cycle_counter;
logic [31:0] cycle_counter_plus1;

enum logic [2:0] {
	s_wait,
	s_count,
	s_end
} state, next_state;

assign seconds_left = (cycles - cycle_counter) / 10000000 / 5;

always_comb
begin
	case(state)
		s_wait:
		begin
			cycle_counter_plus1 = 32'h000;
			Done = 1'b0;
		end
		s_count:
		begin
			cycle_counter_plus1 = cycle_counter + 1'b1;
			Done = 1'b0;
		end
		s_end:
		begin
			cycle_counter_plus1 = 32'h000;
			Done = 1'b1;
		end
		default:
		begin
			cycle_counter_plus1 = 32'h000;
			Done = 1'b0;
		end
	endcase
end

always_comb
begin
	case(state)
		s_wait:
		begin
			if (Start == 1'b1)
			begin
				next_state = s_count;
			end
			else
			begin
				next_state = state;
			end
		end
		s_count:
		begin
			if (cycle_counter >= cycles)
			begin
				next_state = s_end;
			end
			else
			begin
				next_state = state;
			end
		end
		s_end:
		begin
			if (Start == 1'b0)
			begin
				next_state = s_wait;
			end
			else
			begin
				next_state = state;
			end
		end
		default:
		begin
			next_state = state;
		end
	endcase
end

always_ff @(posedge Clk)
begin
	if (Reset == 1'b1)
	begin
		state <= s_wait;
		cycle_counter <= 0;
	end
	else
	begin
		state <= next_state;
		cycle_counter <= cycle_counter_plus1;
	end
end

endmodule // timer