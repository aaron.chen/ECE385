module color_LUT(input logic  [3:0] inColor,
                 output logic [7:0] R_out, G_out, B_out
                 );

  logic [7:0] R, G, B;

  always_comb
  begin

    /*unique case(inColor)
    8'd0:
    begin // black pipe
      R = 8'd0;
      G = 8'd0;
      B = 8'd0;
    end

    8'd1: // green pipe
    begin
      R = 8'd34;
      G = 8'd177;
      B = 8'd76;
    end

    8'd2: // blue background
    begin
      R = 8'd153;
      G = 8'd217;
      B = 8'd234;
    end

    8'd3: // yellow background
    begin
      R = 8'd255;
      G = 8'd242;
      B = 8'd0;
    end

    8'd4: // white background
    begin
      R = 8'd255;
      G = 8'd255;
      B = 8'd255;
    end

    8'd5: // brown background
    begin
      R = 8'd185;
      G = 8'd122;
      B = 8'd87;
    end

    8'd6: // red bird
    begin
      R = 8'd237;
      G = 8'd28;
      B = 8'd36;
    end

    8'd7: // gold bird
    begin
      R = 8'd255;
      G = 8'd201;
      B = 8'd14;
    end
	 endcase*/
	 
  unique case(inColor)
    8'd1:
    begin // black pipe
      R = 8'd0;
      G = 8'd0;
      B = 8'd0;
    end

    8'd2: // green pipe
    begin
      R = 8'd34;
      G = 8'd177;
      B = 8'd76;
    end

    8'd3: // blue background
    begin
      R = 8'd153;
      G = 8'd217;
      B = 8'd234;
    end

    8'd5: // white background
    begin
      R = 8'd255;
      G = 8'd255;
      B = 8'd255;
    end

    8'd7: // red bird
    begin
      R = 8'd237;
      G = 8'd28;
      B = 8'd36;
    end

    8'd8: // gold bird
    begin
      R = 8'd255;
      //G = 8'd201;
      G = 8'd242;
		//B = 8'd14;
      B = 8'd0;
	 end
	 endcase
  end
 
  assign R_out = R;
  assign G_out = G;
  assign B_out = B;

endmodule
