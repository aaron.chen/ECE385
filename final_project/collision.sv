import game_constants::*;

module collision (
	input logic [9:0] ball_a_X_Pos,
	input logic [9:0] ball_a_Y_Pos,

	input logic [9:0] ball_b_X_Pos,
	input logic [9:0] ball_b_Y_Pos,

	output logic is_colliding
);

always_comb
begin
	if ((ball_a_X_Pos - ball_b_X_Pos) <= BALL_SIZE / 2 && (ball_a_X_Pos - ball_b_X_Pos) >= -BALL_SIZE / 2)
	begin
		if ((ball_a_Y_Pos - ball_b_Y_Pos) <= BALL_SIZE / 2 && (ball_a_Y_Pos - ball_b_Y_Pos) >= -BALL_SIZE / 2)
		begin
			is_colliding = 1'b1;
		end
	end
	else
	begin
		is_colliding = 1'b0;
	end
end

endmodule