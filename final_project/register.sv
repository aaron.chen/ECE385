module register #(parameter width = 8)
(
	input logic Clk, Reset,
	input logic Load,
	input logic [width-1:0] data_in,
	output logic [width-1:0] data_out
);

logic [width-1:0] data;

always_ff @(posedge Clk)
begin
	if(Reset)
		data <= 0;
	else
	begin
		if (Load)
			data <= data_in;
	end
end

assign data_out = data;

endmodule : register
	