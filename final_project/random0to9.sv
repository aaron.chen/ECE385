//output 4-bit integer (0 - 9), changes every clk cycle

module random0to9
(
	input logic Clk, Reset,
	input logic [3:0] twister,
	output logic [3:0] out
);

parameter A = 1031;
parameter B = 47297;

logic [15:0] seed;
logic [15:0] next_seed;

assign out = next_seed % 10;

always_ff @(posedge Clk)
begin
	if(Reset)
	begin
		seed <= 0;
	end
	else
	begin
		seed <= next_seed;
	end
end

always_comb
begin
	next_seed = (A * seed) + B + twister;
end

endmodule // random