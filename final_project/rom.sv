/*
 * ECE385-HelperTools/PNG-To-Txt
 * Author: Rishi Thakkar
 *
 */

module frameROM
(
		input [18:0] addr,
		input Clk,

		output logic [3:0] data_Out
);

// mem has width of 4 bits and a total of 400 addresses
logic [3:0] mem [0:4899];

initial
begin
	 $readmemh("sprite_bytes/down.txt", mem);
end


always_ff @ (posedge Clk) begin
	data_Out<= mem[addr];
end

endmodule
