import game_constants::*;
import digit_sprites::*;

module scoreboard (
	input logic Clk,    // Clock
	input logic Reset,
	input logic Load,

	input logic DrawX, DrawY // Current pixel coordinates
	output logic is_scoreboard, // Tells color mapper if current pixel belongs to scoreboard or background

	input logic [9:0] score_in, //max 999	
);

decimal_digit hundreds_digit_out, tens_digit_out, ones_digit_out;
logic [3:0] hundreds_digitmux_sel, tens_digitmux_sel, ones_digitmux_sel;

logic [6:0] score_div_10; 
logic [3:0] score_div_100; 

assign score_div_10 = score_in / 10;
assign score_div_100 = score_in / 100;

register #(.width(DIGIT_NUM_PIXELS)) hundreds_digit
(
	.Clk,
	.Reset,
	.Load,
	.data_in(hundreds_digitmux_out),
	.data_out(hundreds_digit_out)
);

register #(.width(DIGIT_NUM_PIXELS)) tens_digit
(
	.Clk,
	.Reset,
	.Load,
	.data_in(tens_digitmux_out),
	.data_out(tens_digit_out)
);

register #(.width(DIGIT_NUM_PIXELS)) ones_digit
(
	.Clk,
	.Reset,
	.Load,
	.data_in(ones_digitmux_out),
	.data_out(ones_digit_out)
);

mux10to1 #(.width(DIGIT_NUM_PIXELS)) hundreds_digitmux
(
	.sel(hundreds_digitmux_sel),
	.a9(NINE_DIGIT),
	.a8(EIGHT_DIGIT), 
	.a7(SEVEN_DIGIT),
	.a6(SIX_DIGIT),
	.a5(FIVE_DIGIT),
	.a4(FOUR_DIGIT),
	.a3(THREE_DIGIT),
	.a2(TWO_DIGIT),
	.a1(ONE_DIGIT),
	.a0(ZERO_DIGIT),
	.out(hundreds_digitmux_out)
);

mux10to1 #(.width(DIGIT_NUM_PIXELS)) tens_digitmux
(
	.sel(tens_digitmux_sel),
	.a9(NINE_DIGIT),
	.a8(EIGHT_DIGIT), 
	.a7(SEVEN_DIGIT),
	.a6(SIX_DIGIT),
	.a5(FIVE_DIGIT),
	.a4(FOUR_DIGIT),
	.a3(THREE_DIGIT),
	.a2(TWO_DIGIT),
	.a1(ONE_DIGIT),
	.a0(ZERO_DIGIT),
	.out(tens_digitmux_out)
);

mux10to1 #(.width(DIGIT_NUM_PIXELS)) ones_digitmux
(
	.sel(ones_digitmux_sel),
	.a9(NINE_DIGIT),
	.a8(EIGHT_DIGIT), 
	.a7(SEVEN_DIGIT),
	.a6(SIX_DIGIT),
	.a5(FIVE_DIGIT),
	.a4(FOUR_DIGIT),
	.a3(THREE_DIGIT),
	.a2(TWO_DIGIT),
	.a1(ONE_DIGIT),
	.a0(ZERO_DIGIT),
	.out(ones_digitmux_out)
);

always_comb
begin
	hundreds_digitmux_sel = 0;
	tens_digitmux_sel = 0;
	ones_digitmux_sel = 0;
	
	if (score_in > MAX_SCORE)
	begin
		hundreds_digitmux_sel = 9;
		tens_digitmux_sel = 9;
		ones_digitmux_sel = 9;
	end
	else
	begin
		hundreds_digitmux_sel = score_div_100 % 10;
		tens_digitmux_sel = score_div_10 % 10;
		ones_digitmux_sel = score_in % 10;
	end
end

endmodule