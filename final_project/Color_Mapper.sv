//-------------------------------------------------------------------------
//    Color_Mapper.sv                                                    --
//    Stephen Kempf                                                      --
//    3-1-06                                                             --
//                                                                       --
//    Modified by David Kesler  07-16-2008                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Modified by Po-Han Huang  10-06-2017                               --
//                                                                       --
//    Fall 2017 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 8                                         --
//    University of Illinois ECE Department                              --
//-------------------------------------------------------------------------

// color_mapper: Decide which color to be output to VGA for each pixel.
module color_mapper
(
	input logic Clk,
	input logic [8:0] draw_user1_ball_arr,            // Whether current pixel belongs to ball 
	input logic [8:0] collision_arr,           
	input logic [8:0] exist_arr,           
	input logic [9:0] DrawX, DrawY,       // Current pixel coordinates
	input logic [9:0] position_x_arr [8:0],
	input logic [9:0] position_y_arr [8:0],
	output logic [7:0] VGA_R, VGA_G, VGA_B // VGA RGB output
);

frameROM sprite_info_rom
(
	.addr,
	.Clk,
	.data_Out(sprite_info)
);

logic [7:0] Red, Green, Blue;
logic [18:0] addr;
logic [3:0] sprite_info;
// int Bird_W = 50;
// int Bird_H = 50;

// Output colors to VGA
assign VGA_R = Red;
assign VGA_G = Green;
assign VGA_B = Blue;
 
// logic Bird_On; 
// logic [9:0] Draw_Pixel_Bird_X,   Draw_Pixel_Bird_Y;
// assign Draw_Pixel_Bird_X = DrawX;
// assign Draw_Pixel_Bird_Y = DrawY;

// logic [7:0] Bird_R, Bird_G, Bird_B;
// logic [3:0] Bird_Color;
// bird_ROM BR0 (.x(Draw_Pixel_Bird_X), .y(Draw_Pixel_Bird_Y), .out(Bird_Color));
// color_LUT CL0(.inColor(Bird_Color), .R_out(Bird_R), .G_out(Bird_G), .B_out(Bird_B));
    
    // Assign color based on is_ball signal
always_comb
begin
	//white ball;
	Red = 8'hff;
	Green = 8'hff;
	Blue = 8'hff;
	
	addr = (DrawY - position_y_arr[0]) * 70 +  (DrawX - position_x_arr[0]);

	if (draw_user1_ball_arr[0] && exist_arr[0] && sprite_info != 0)
	begin
		case(sprite_info)
		1:
		begin
			Red = 8'h9d;
			Green = 8'hc7;
			Blue = 8'h54;
		end
		2:
		begin
			Red = 8'hff;
			Green = 8'h51;
			Blue = 8'h79;
		end
		3:
		begin
			Red = 8'hff;
			Green = 8'hc7;
			Blue = 8'h5c;
		end
		4:
		begin
			Red = 8'ha2;
			Green = 8'hff;
			Blue = 8'hff;
		end
		5:
		begin
			Red = 8'hff;
			Green = 8'h96;
			Blue = 8'hb7;
		end
		6:
		begin
			Red = 8'h00;
			Green = 8'h00;
			Blue = 8'h00;
		end
		7:
		begin
			Red = 8'h56;
			Green = 8'h6e;
			Blue = 8'h2c;
		end
		8:
		begin
			Red = 8'h8f;
			Green = 8'h2c;
			Blue = 8'h43;
		end
		9:
		begin
			Red = 8'ha8;
			Green = 8'h86;
			Blue = 8'h3c;
		end
		10:
		begin
			Red = 8'ha2;
			Green = 8'hb9;
			Blue = 8'hb9;
		end
		11:
		begin
			Red = 8'ha8;
			Green = 8'h64;
			Blue = 8'h7b;
		end
		endcase
	end
	else if (draw_user1_ball_arr[1] && exist_arr[1]) 
	begin
		if (collision_arr[1])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[2] && exist_arr[2]) 
	begin
		if (collision_arr[2])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[3] && exist_arr[3]) 
	begin
		if (collision_arr[3])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[4] && exist_arr[4]) 
	begin
		if (collision_arr[4])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[5] && exist_arr[5])
	begin
		if (collision_arr[5])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[6] && exist_arr[6]) 
	begin
		if (collision_arr[6])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[7] && exist_arr[7]) 
	begin
		if (collision_arr[7])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else if (draw_user1_ball_arr[8] && exist_arr[8]) 
	begin
		if (collision_arr[8])
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		end
		else
		begin
			//white ball;
			Red = 8'hff;
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
	else 
	begin
		// Background with nice color gradient
		if ((DrawY <= 50) || (DrawY >= 429) ||(DrawX <= 50) || (DrawX >= 589))
		begin
			Red = 8'h00; 
			Green = 8'h00;
			Blue = 8'h00;
		end
		else if ((DrawY <= 429) && (DrawY >= 409))
		begin
			Red = 8'hA5; 
			Green = 8'h2A;
			Blue = 8'h2A;
		end
		else
		begin
			Red = 8'h10; 
			Green = 8'hff;
			Blue = 8'hff;
		end
	end
end

endmodule
